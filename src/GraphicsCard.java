public class GraphicsCard {
    private IGraphicsSetting iGraphicsCard;


    public void setiGraphicsCard(IGraphicsSetting iGraphicsCard) {
        this.iGraphicsCard = iGraphicsCard;
    }

    public void podajMoc() {
        int zmienna = iGraphicsCard.getNeededProcessingPower();
        System.out.println("potrzebuje: " + zmienna + " mocy");

    }
}
