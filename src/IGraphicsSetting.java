public interface IGraphicsSetting {
    int getNeededProcessingPower();
}
